plugins {
    idea
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(project(":ktstrings"))
}
